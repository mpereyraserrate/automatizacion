package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SecurePage {
    private WebDriver webDriver;
    private By alertdiv = By.xpath("//*[@id=\"flash\"]");


    public SecurePage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public boolean isLoginMessageDisplayed(){
        return webDriver.findElement(alertdiv).isDisplayed();
    }

    public boolean getPanelMessage(String mensaje){
        return webDriver.findElement(alertdiv).getText().contains(mensaje) ;
    }

}
