package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import pages.LoginPage;
import pages.SecurePage;

public abstract  class BaseTest {
    protected WebDriver webDriver;

    @BeforeMethod
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","resource/chromedriver.exe");
        webDriver = new ChromeDriver();
        webDriver.get("https://the-internet.herokuapp.com/login");
       // LoginPage loginPage = new LoginPage(webDriver);
       // SecurePage securepage = loginPage.loginAs("tomsmith","SuperSecretPassword!");
       // employeePage.addEmployee("Juan","Juan@gmail.com","MTZ","58899624");

       // webDriver.quit();
    }

    /*public static void main(String arg[]){
        BaseTest baseTest = new BaseTest();
        baseTest.setUp();
    }*/
}
