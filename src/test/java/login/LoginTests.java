package login;

import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.SecurePage;

public class LoginTests extends BaseTest {

    @Test
    public void testsuccesslogin(){
        LoginPage loginpage = new LoginPage(webDriver);
        SecurePage securePage = loginpage.loginAs("tomsmith","SuperSecretPassword!");
        Assert.assertTrue(securePage.isLoginMessageDisplayed());
        Assert.assertTrue(securePage.getPanelMessage("You logged into a secure area!"),"You logged into a secure area!");

    }




}
